﻿use sa2_romochivska

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Exams')
BEGIN
    DROP TABLE Exams;
END;

CREATE TABLE Exams (
    Id INT PRIMARY KEY IDENTITY(1,1),
    LastName NVARCHAR(50) NOT NULL,
    GroupName NVARCHAR(50) NOT NULL,
    Subject NVARCHAR(50) NOT NULL,
    TicketNumber INT NOT NULL,
    Grade INT NOT NULL,
    Teacher NVARCHAR(50) NOT NULL
);

INSERT INTO Exams(LastName, GroupName, Subject, TicketNumber, Grade, Teacher) VALUES
    ('John Doe', 'GroupA', 'Math', 123, 85, 'Professor Smith'),
    ('Jane Smith', 'GroupB', 'History', 456, 92, 'Professor Johnson'),
    ('Bob Johnson', 'GroupC', 'Physics', 789, 78, 'Professor Davis'),
    ('Alice Brown', 'GroupA', 'Chemistry', 987, 95, 'Professor White'),
    ('Charlie White', 'GroupB', 'Biology', 654, 88, 'Professor Taylor'),
    ('Zoe Davis', 'GroupC', 'English', 231, 75, 'Professor Martin'),
    ('David Lee', 'GroupA', 'Computer Science', 876, 91, 'Professor Turner'),
    ('Eva Taylor', 'GroupB', 'Geography', 543, 84, 'Professor Adams'),
    ('Frank Miller', 'GroupC', 'Economics', 321, 89, 'Professor Carter'),
    ('Grace Wilson', 'GroupA', 'Psychology', 789, 93, 'Professor Turner'),
    ('Henry Turner', 'GroupB', 'Sociology', 876, 80, 'Professor Clark'),
    ('Ivy Clark', 'GroupC', 'Political Science', 234, 87, 'Professor Moore'),
    ('Jack Moore', 'GroupA', 'Art', 567, 76, 'Professor Hall'),
    ('Kelly Hall', 'GroupB', 'Music', 890, 94, 'Professor Davis'),
    ('Leo Davis', 'GroupC', 'Physical Education', 123, 82, 'Professor Lee'),
    ('Mia Martin', 'GroupA', 'Literature', 456, 97, 'Professor Adams'),
    ('Nathan Scott', 'GroupB', 'Philosophy', 789, 79, 'Professor Turner'),
    ('Olivia Adams', 'GroupC', 'Communication', 321, 86, 'Professor Carter'),
    ('Paula Carter', 'GroupA', 'Foreign Language', 654, 88, 'Professor Turner'),
    ('Quinn Turner', 'GroupB', 'Environmental Science', 987, 90, 'Professor Turner');

SELECT * FROM Exams;


SELECT * FROM Exams WHERE Subject LIKE 'History%';

SELECT * FROM Exams WHERE Teacher IS NULL;

SELECT * FROM Exams WHERE GroupName = 'GroupA' OR GroupName = 'GroupB';


SELECT * FROM Exams WHERE Grade BETWEEN 80 AND 90;


SELECT * FROM Exams WHERE Grade > 90 AND GroupName = 'GroupA';


SELECT DISTINCT GroupName FROM Exams;


SELECT LastName, Grade, Grade * 2 AS DoubledGrade FROM Exams;


SELECT GroupName, AVG(Grade) AS AverageGrade FROM Exams GROUP BY GroupName;

SELECT * FROM Exams ORDER BY LastName ASC;

SELECT * FROM Exams ORDER BY LastName DESC;

UPDATE Exams SET Grade = 88 WHERE LastName = 'John Doe';

SELECT * FROM Exams WHERE LastName = 'John Doe';